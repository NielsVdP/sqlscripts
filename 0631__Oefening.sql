USE ModernWays;

SELECT DISTINCT Voornaam
FROM studenten
WHERE voornaam IN (SELECT voornaam FROM directieleden) 
AND voornaam IN  (SELECT voornaam FROM personeelsleden);