USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumRelease`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumRelease` ()
BEGIN
    DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0;
    DECLARE randomBandId INT DEFAULT 0;
    
    SELECT COUNT(*) INTO numberOfAlbums FROM albums;
    SELECT COUNT(*) INTO numberOfBands FROM bands;    
    SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
    SET randomBandId = FLOOR(RAND() * numberOfBands) + 1;
    
    IF NOT (randomBandId, randomAlbumId) IN (SELECT bands_id, albums_id FROM albumreleases WHERE bands_id = randomBandId AND randomAlbumId = albums_id) THEN
		START TRANSACTION;
		INSERT INTO albumreleases (bands_id, albums_id)
        VALUES (randomBandId, randomAlbumId);
        COMMIT;
	END IF;
END$$
DELIMITER ;