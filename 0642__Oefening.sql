USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CleanupOldMemberships` (IN someDate DATE, OUT numberCleaned INT)
BEGIN
	START TRANSACTION;
    SELECT 
		COUNT(einddatum)
	INTO numberCleaned
    FROM 
        lidmaatschappen
    WHERE
		einddatum IS NOT NULL AND einddatum < someDate;
	
    SET SQL_SAFE_UPDATES = 0;
		
		DELETE 
		FROM 
			lidmaatschappen
		WHERE
			einddatum IS NOT NULL AND einddatum < someDate;
		
	SET SQL_SAFE_UPDATES = 1;
    COMMIT;
END$$
DELIMITER ;