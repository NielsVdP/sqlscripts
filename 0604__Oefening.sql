USE ModernWays;

CREATE TABLE Uitleningen (
	Startdatum DATE NOT NULL,
    Einddatum DATE,
    Leden_Id INT NOT NULL,
    Boeken_Id INT NOT NULL,
	CONSTRAINT fk_Uitleningen_Leden 
		FOREIGN KEY (Boeken_Id) 
		REFERENCES Boeken(Id),
	CONSTRAINT fk_Uitleningen_Boeken 
		FOREIGN Key (Leden_Id) 
		REFERENCES Leden(Id)
);
        
