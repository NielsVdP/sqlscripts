USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `NumberOfGenres` (OUT Aantal TINYINT)
BEGIN
    SELECT DISTINCT COUNT(naam)
    INTO Aantal
    FROM genres;    
END$$
DELIMITER ;