USE ModernWays;

CREATE VIEW AuteursBoekenRatings AS
SELECT Auteur, Titel, Rating
FROM auteursboeken 
INNER JOIN gemiddelderatings ON auteursboeken.Boeken_Id = gemiddelderatings.Boeken_Id;
