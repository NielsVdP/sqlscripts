USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
    DECLARE randomNumber INT DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLSTATE  '45002' SELECT "State 45002 opgevangen. Geen probleem."  AS message;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SELECT "Een algemene fout opgevangen."  AS message;
    
    SET randomNumber = FLOOR(RAND() * 3)+1;
    
    IF randomNumber = 1 THEN
		SIGNAL SQLSTATE  '45001';
    ELSEIF randomNumber = 2 THEN
		SIGNAL SQLSTATE  '45002';
    ELSEIF randomNumber = 3 THEN
		SIGNAL SQLSTATE  '45003';
    END IF;
END$$
DELIMITER ;