USE ModernWays;

ALTER TABLE Baasjes 
ADD COLUMN Huisdieren_Id INT,
ADD CONSTRAINT fk_Baasjes_Baasje
FOREIGN KEY (Huisdieren_Id)
REFERENCES Huisdieren(Id);
SELECT * FROM Baasjes;