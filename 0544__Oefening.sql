USE ModernWays;

SELECT Artiest, SUM(Aantalbeluisteringen) AS "Totaal aantal beluisteringen"
FROM Liedjes
GROUP BY Artiest
HAVING Length(Artiest) >= 10;