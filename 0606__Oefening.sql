USE ModernWays;

SELECT Leden.Voornaam, Boeken.Titel
FROM Uitleningen
	INNER JOIN Leden ON Leden.Id = Uitleningen.Leden_Id
    INNER JOIN Boeken ON Boeken.Id = Uitleningen.Boeken_Id