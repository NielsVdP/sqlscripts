USE ModernWays;

INSERT INTO Personen (
	Voornaam, Familienaam
)
VALUES (
	'Jean-Paul', 'Sartre'
);

INSERT INTO Boeken (
   Titel,
   Verschijningsdatum,
   Uitgeverij,
   Personen_Id
)
VALUES (
   'De Woorden',
   '1961',
   'De Bezige Bij',
   (SELECT Id FROM Personen WHERE
       Familienaam = 'Sartre' AND Voornaam = 'Jean-Paul'));