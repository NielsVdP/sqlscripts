USE ModernWays;

SELECT voornaam 
FROM studenten
WHERE LENGTH(voornaam) < (SELECT AVG(LENGTH(voornaam)) FROM studenten);