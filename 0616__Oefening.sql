USE ModernWays;

CREATE VIEW GemiddeldeRatings AS
SELECT reviews.Boeken_Id, AVG(reviews.Rating) AS Rating
FROM reviews GROUP BY reviews.Boeken_Id;