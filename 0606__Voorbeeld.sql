USE ModernWays;

SELECT Games.Id, Platformen.Id, Releases.Datum
FROM (Releases
	INNER JOIN Games ON Games_Id = Games.Id
	INNER JOIN Platformen ON Platformen_Id = Platformen.Id)
ORDER BY Games.Id, Platformen.Id ASC;
