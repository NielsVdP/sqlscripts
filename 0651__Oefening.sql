USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration`;

DELIMITER $$
CREATE PROCEDURE `GetAlbumDuration` (
	IN albumId INT, OUT totalDuration SMALLINT UNSIGNED)
BEGIN
	DECLARE ok BOOL DEFAULT 1;
	DECLARE songDuration TINYINT UNSIGNED DEFAULT 0;	

	DECLARE cursorCurrentSong
	CURSOR FOR SELECT lengte FROM liedjes WHERE albums_id = albumId;

	DECLARE CONTINUE HANDLER
	FOR NOT FOUND SET ok = 0;
    
	SET totalDuration = 0;    
	OPEN cursorCurrentSong;
		getSongDuration: LOOP
			FETCH cursorCurrentSong INTO songDuration;
			IF ok = 0 THEN 
				LEAVE getSongDuration;
			END IF;
			SET totalDuration = songDuration + totalDuration;
		END LOOP getSongDuration;
	CLOSE cursorCurrentSong;
END$$
DELIMITER ;