USE ModernWays;

INSERT INTO BoekenNaarAuteurs (Boeken_Id,Auteurs_Id)
VALUES
(1,10), -- Boek 1 is Norwegian Wood, auteur 10 is Haruki Murakami
(2,10),
(3, 16), -- Neil Gaiman
(4, 16), -- Neil Gaiman
(5, 17), -- Stephen King
(6, 18), -- Terry Pratchett
(6, 16), -- Neil Gaiman
(7, 17), -- Stephen King
(7, 19); -- Peter Straub
