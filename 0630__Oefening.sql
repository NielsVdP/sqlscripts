USE ModernWays;

-- Hetzelfde resultaat ??
-- 
-- SELECT voornaam, familienaam
-- FROM studenten, verenigingrollen
-- GROUP BY studenten.id, verenigingrollen.studenten_id
-- HAVING studenten.Id = verenigingrollen.studenten_id;

SELECT Voornaam, Familienaam
FROM studenten
WHERE studenten.id IN (SELECT studenten_id FROM verenigingrollen)