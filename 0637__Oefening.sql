USE ModernWays;

-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
DROP TABLE IF EXISTS Personen;
CREATE TABLE Personen (
    Voornaam VARCHAR(255) NOT NULL,
    Familienaam VARCHAR(255) NOT NULL
);

-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
-- we gebruiken hiervoor een subquery
INSERT INTO Personen (Voornaam, Familienaam)
	SELECT DISTINCT Voornaam, Familienaam FROM Boeken;

-- extra kolommen en primary key toevoegen aan tabel Personen 
ALTER TABLE Personen ADD (
	Id INT AUTO_INCREMENT PRIMARY KEY,
   AanspreekTitel VARCHAR(30) NULL,
   Straat VARCHAR(80) NULL,
   Huisnummer VARCHAR (5) NULL,
   Stad VARCHAR (50) NULL,
   Commentaar VARCHAR (100) NULL,
   Biografie VARCHAR(400) NULL);
   
-- kolom met foreign key toevoegen aan tabel Boeken
-- zonder "not null" constraint om foutmelding te voorkomen
-- om tabel boeken en Personen te linken
ALTER TABLE Boeken ADD Personen_Id INT NULL;

-- foreign key kolom van tabel Boeken invullen
SET SQL_SAFE_UPDATES = 0;
UPDATE Boeken CROSS JOIN Personen
    SET Boeken.Personen_Id = Personen.Id
	WHERE Boeken.Voornaam = Personen.Voornaam AND
    Boeken.Familienaam = Personen.Familienaam;
SET SQL_SAFE_UPDATES = 1;

-- foreign key verplicht maken
ALTER TABLE Boeken CHANGE Personen_Id Personen_Id INT NOT NULL;

-- overbodige kolommen verwijderen
ALTER TABLE Boeken DROP COLUMN Voornaam, 
	DROP COLUMN Familienaam;
    
-- dan de constraint toevoegen
ALTER TABLE Boeken ADD CONSTRAINT fk_Boeken_Personen
	FOREIGN KEY (Personen_Id) REFERENCES Personen(Id);