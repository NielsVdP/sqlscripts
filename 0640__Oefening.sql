USE `aptunes`;
DROP procedure IF EXISTS `GetLiedjes`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetLiedjes` (IN input VARCHAR(50))
BEGIN
    SELECT 
        Titel
    FROM 
        liedjes
    WHERE
		titel LIKE CONCAT('%', input,'%');
END$$
DELIMITER ;

CALL GetLiedjes('Synchronized')