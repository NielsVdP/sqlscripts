USE ModernWays;

ALTER VIEW AuteursBoeken 
AS SELECT CONCAT(Personen.Voornaam, ' ', Personen.Familienaam) AS Auteur, Titel, Boeken.Id AS Boeken_Id
FROM Boeken 
INNER JOIN Publicaties ON BOEKEN.Id = Publicaties.Boeken_Id
INNER JOIN Personen ON Publicaties.Personen_Id = Personen.Id;