USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration2`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration2` (
	IN album_Id INT, OUT totalDuration SMALLINT UNSIGNED)
SQL SECURITY INVOKER
BEGIN
	DECLARE ok BOOL DEFAULT 1;
	DECLARE songDuration TINYINT UNSIGNED;	

	DECLARE currentSong
	CURSOR FOR SELECT lengte FROM liedjes WHERE liedjes.albums_id = album_Id;

	DECLARE CONTINUE HANDLER
	FOR NOT FOUND SET ok = 0;
    
	SET totalDuration = 0;    
	OPEN currentSong;
		getSongDuration: LOOP
			FETCH currentSong INTO songDuration;
			IF ok = 0 THEN LEAVE getSongDuration;
			END IF;
			SET totalDuration = songDuration + totalDuration;
		END LOOP getSongDuration;
	CLOSE currentSong;
END$$
DELIMITER ;