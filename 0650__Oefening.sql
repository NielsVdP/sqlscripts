USE `aptunes`;
DROP procedure IF EXISTS `DangerousInsertAlbumreleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DangerousInsertAlbumreleases` ()
BEGIN
	DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    
    DECLARE randomAlbumId1 INT DEFAULT 0;
    DECLARE randomBandId1 INT DEFAULT 0;
    DECLARE randomAlbumId2 INT DEFAULT 0;
    DECLARE randomBandId2 INT DEFAULT 0;
    DECLARE randomAlbumId3 INT DEFAULT 0;
    DECLARE randomBandId3 INT DEFAULT 0;
    DECLARE randomNumber TINYINT DEFAULT 0;
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
    BEGIN		
        ROLLBACK;
		SELECT "Nieuwe releases konden niet worden toegevoegd.";
        -- RESIGNAL SET MESSAGE_TEXT = 'Nieuwe releases konden niet worden toegevoegd.';
    END;
    SELECT COUNT(*) INTO numberOfAlbums FROM albums;
    SELECT COUNT(*) INTO numberOfBands FROM bands;        
    
    SET randomAlbumId1 = FLOOR(RAND() * numberOfAlbums) + 1;
	SET randomBandId1 = FLOOR(RAND() * numberOfBands) + 1;    
	SET randomAlbumId2 = FLOOR(RAND() * numberOfAlbums) + 1;
	SET randomBandId2 = FLOOR(RAND() * numberOfBands) + 1;    
	SET randomAlbumId3 = FLOOR(RAND() * numberOfAlbums) + 1;
	SET randomBandId3 = FLOOR(RAND() * numberOfBands) + 1;    
    
    START TRANSACTION;		        
		INSERT INTO albumreleases (Bands_Id,Albums_Id)
		VALUES 
		(randomBandId1, randomAlbumId1), 
		(randomBandId2, randomAlbumId2);
		SET randomNumber = FLOOR(RAND() * 3)+1;
		IF randomNumber = 1 THEN
			SIGNAL SQLSTATE '45000';
		END IF;
		INSERT INTO albumreleases (Bands_Id,Albums_Id)
		VALUES 
		(randomBandId3, randomAlbumId3);
    COMMIT;
END$$
DELIMITER ;