USE ModernWays;

CREATE VIEW AuteursBoeken
AS
SELECT CONCAT(Personen.Voornaam, ' ',Personen.Familienaam) AS Auteur, Boeken.Titel AS Titel
FROM Boeken
	INNER JOIN Publicaties ON Boeken.Id = Publicaties.Boeken_Id
    INNER JOIN Personen ON Publicaties.Personen_Id = Personen.Id;
	